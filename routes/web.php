<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/products', function () {
    return view('products');
})->name('products');

Route::get('/products/{name}', function ($name) {
    return view('single-product', ['name' => $name]);
})->name('product.details');

Route::get('/demo/{name}', function ($name) {
    return view('demo', ['name' => $name]);
})->name('product.demo');

Route::get('/privacy', function () {
    return view('privacy');
})->name('privacy');

Route::get('/terms', function () {
    return view('terms');
})->name('terms');

Route::get('/refund', function () {
    return view('refund');
})->name('refund');

Route::get('/payment/success/{name}', function ($name) {
    return view('payment-success', ['productName' => $name]);
})->name('payment.success');
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
