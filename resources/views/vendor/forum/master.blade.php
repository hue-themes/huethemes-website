<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/all.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145823004-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-145823004-1');
    </script>

</head>
<body>
    <div id="page">
        <div id="app">
            <nav class="fh5co-nav {{Request::is('/') ? '' : 'header-normal'}}" role="navigation">
                <div class="top-menu">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-10 col-sm-4">
                                <div id="fh5co-logo">
                                    <a href="{{ URL::route('welcome') }}">
                                        <span class="logo-h">H</span><span class="logo-u">u</span><span class="logo-e">e</span>Themes<img src="images/logo-icon.svg"/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-8 text-right menu-1">
                                <ul>
                                    <li class="active" ><a href="{{ URL::route('welcome') }}">Home</a></li>
                                    <li><a href="{{ URL::route('products') }}">Products</a></li>
                                    <li><a href="{{ URL::to( '/'.config('forum.routing.prefix')) }}">Froums</a></li>
                                    <li><a href="{{ URL::route('contact') }}">Contact</a></li>
                                    <li class="btn-cta"><a href="{{ URL::route('login') }}"><span>Login</span></a></li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </nav>
            
            <div id="fh5co-contact">
                <div class="container">
                        @yield('content')
                </div>
            </div>
            <footer id="fh5co-footer" role="contentinfo" style="background-image: url(images/img_bg_4.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row row-pb-md">
                        <div class="col-md-4 col-md-offset-2 fh5co-widget">
                            <h3>About HueThemes</h3>
                            <p>We at HueThemes create top quality WordPress themes and plugins.</p>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 fh5co-widget">
                            <h3>Links</h3>
                            <ul class="fh5co-footer-links">
                                <li><a href="{{ URL::route('login') }}">Login</a></li>
                                <li><a href="{{ URL::to( '/'.config('forum.routing.prefix')) }}">Support Forum</a></li>
                                <li><a href="{{ URL::route('register') }}">Register</a></li>
                            </ul>
                        </div>

                        <!-- <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
                            <h3>Learn &amp; Grow</h3>
                            <ul class="fh5co-footer-links">
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Testimonials</a></li>
                                <li><a href="#">Handbook</a></li>
                                <li><a href="#">Held Desk</a></li>
                            </ul>
                        </div>

                        <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
                            <h3>Engage us</h3>
                            <ul class="fh5co-footer-links">
                                <li><a href="#">Marketing</a></li>
                                <li><a href="#">Visual Assistant</a></li>
                                <li><a href="#">System Analysis</a></li>
                                <li><a href="#">Advertise</a></li>
                            </ul>
                        </div> -->

                        <div class="col-md-2 col-sm-4 col-xs-6 fh5co-widget">
                            <h3>Legal</h3>
                            <ul class="fh5co-footer-links">
                                <li><a href="{{ URL::route('privacy') }}">Privacy Policy</a></li>
                                <li><a href="{{ URL::route('terms') }}">Terms and Conditions</a></li>
                                <li><a  href="{{ URL::route('refund') }}">Refund Policy</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="row copyright">
                        <div class="col-md-12 text-center">
                            <p>
                                <small class="block">&copy; 2019 HueThemes, a RKS Games Studio Company. All Rights Reserved.</small> 
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>
