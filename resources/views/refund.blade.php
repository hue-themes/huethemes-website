@extends('layouts.app')

@section('content')
<div id="fh5co-contact">
	<div class="container">
		<div class="row">
            <div class="col-md-12">
                <h2>Refund policy</h2>
                <hr/>
                <div class="article-content">
                    <p>We are always looking forward to hearing feedbacks from our customers, In case our customers face any issue while using our themes, they can raise a support ticket which are resolved on a high priority basis.</p>

                    <p>Please read the following refund policy for HueThemes products.</p>

                    <strong>HueThemes provides full refund of your payment in the following cases:</strong>
                    <p>
                    <ul>
                    <li>When you stably encounter an error which is preventing you from using our theme properly, and if we are not able to correct the error within an acceptable period of time or are unable to suggest a temporary solution.</li>
                    <li><b>Please, NOTE!</b> In this case refund is only given if you provide detailed information about your error, requested by our support staff, such as screenshots of error messages, log files, detailed descriptions of your actions, file samples, etc.</li>
                    <li>if you accidentally bought the software more than once.</li>
                    </ul>
                    </p>

                    <strong>We reserve the right to decline refund requests in the following cases:</strong>
                    <p>
                    <ul>
                    <li>when a user demands a refund immediately after buying the software</li>

                    <li>when a user informs us about changing his decision to buy the software, saying he has uninstalled it and is not going to use it without giving any particular reasons.</li>

                    <li>when reasons which prevent a customer from proper using of our software are stated clearly on our official site. (for example, if the customer requests a refund due to the absence of features which we never claimed to provide and which are not listed in the feature list for our software).
                    <br/>
                    In such cases, if you disagree with our grounds for refund refusal, you can apply to your bank and initiate a chargeback.</li>
                    </ul>
                    </p>
                    <p>
                    <b>WARNING!</b> In any case of refund or chargeback your registration name will be immediately blocked and you will not be able to use the new versions of our themes.</p> 
                </div>
            </div>
		</div>
		
	</div>
</div>
@endsection
