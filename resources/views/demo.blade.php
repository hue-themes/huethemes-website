@extends('layouts.demo')

@section('content')
<iframe src="{{ config('demo.url', 'https://demos.huethemes.com').'/'.$name }}">
</iframe>
@endsection
