@extends('layouts.app')

@section('content')
<div id="fh5co-contact">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-2 animate-box">
				<h3>Register</h3>
				<form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="row form-group">
						<div class="col-md-12">
							<label for="email">Name</label>
                            <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Your name">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-12">
							<label for="email">Email</label>
                            <input type="text" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Your email address">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
					</div>

					<div class="row form-group">
						<div class="col-md-12">
							<label for="subject">Password</label>
                            <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password"  autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
                    </div>

                    <div class="row form-group">
						<div class="col-md-12">
							<label for="subject">Confirm Password</label>
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Confirm Password"  autocomplete="new-password">
						</div>
                    </div>
					<div class="form-group">
                        <input type="submit" value="Register" class="btn btn-primary">
					</div>

				</form>		
			</div>
			<div class="col-md-4 animate-box" style="text-align: center;">
				<h2>Login Using</h2>
			</div>
		</div>
		
	</div>
</div>
@endsection
