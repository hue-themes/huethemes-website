<div class="col-md-6">
    <a href="{{ URL::route('product.details', array('name'=>'huefab-premium')) }}">
        <img class="img-responsive" src="{{URL::to('/')}}/images/img_bg_2.png" alt="Free HTML5 Bootstrap Template">
    </a>
</div>
<div class="col-md-6 animate-box product-details course">
    <a href="{{ URL::route('product.details', array('name'=>'huefab-premium')) }}">
        <span>WordPress Theme</span>
        <h2>HueFab Premium | WooCommerce Theme</h2>
    </a>
    <a href="{{ URL::route('product.details', array('name'=>'huefab-premium')) }}">
        <p>HueFab Premium is premium WordPress theme with WooCommerce support. HueFab is especially suited for Ecommerce websites selling Fashion Products. HueFab Premium also includes excellent commercial support.</p>
    </a>
    <div class="desc">
        <span><a href="{{ URL::route('product.details', array('name'=>'huefab-premium')) }}" class="btn btn-primary btn-sm btn-course">Details</a></span>
    </div>
</div>