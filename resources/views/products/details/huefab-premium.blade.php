<div class="row">
    <div class="col-md-6 product-image">
        <a href="{{ URL::route('product.details', array('name'=>'huefab')) }}">
            <img class="img-responsive" src="{{URL::to('/')}}/images/img_bg_2.png" alt="Free HTML5 Bootstrap Template">
        </a>
        <div class="preview-bar course">
            <div class="desc">
                <span><a href="{{ URL::route('product.demo', array('name'=>'huefab-premium')) }}" class="btn btn-primary btn-sm btn-course">Live Demo</a></span>
            </div>
        </div>
    </div>
    <div class="col-md-6 animate-box product-details">
        <a href="{{ URL::route('product.details', array('name'=>'huefab')) }}">
            <span>WordPress Theme</span>
            <h2>HueFab Premium | WooCommerce Theme</h2>
        </a>
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                <h2>$29/year</h2>
                <h4>7 days free trial</h4>
                <h2>comming soon</h2>
            </div>
        </div>
        {{-- <div class="paypal-button">
            <div id="paypal-button-container"></div>
            <script src="https://www.paypal.com/sdk/js?client-id=AW9CwSWB1FQUdPOX8vaYtw7O3B6V3F-9Xnt6GqLZ9MSCPESJ1ykjfmVovC1UU7kpxW_v3h9gKlQHSJnW&vault=true" data-sdk-integration-source="button-factory"></script>
            <script>
            var postPaymentUrl = '{{URL::route('payment.success', array('name'=>'huefab-premium'))}}';
            paypal.Buttons({
                style: {
                    shape: 'rect',
                    color: 'gold',
                    layout: 'vertical',
                    label: 'paypal'
                },
                createSubscription: function(data, actions) {
                    return actions.subscription.create({
                    'plan_id': 'P-37Y888406W579432YL6I7AAQ'
                    });
                },
                onApprove: function(data, actions) {
                    window.location.href = postPaymentUrl;
                }
            }).render('#paypal-button-container');
            </script>
        </div> --}}
    </div>
</div>
<div class="row animate-box">
    <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
        <h2>Features</h2>
        <p>
            <b>HueFab Premium</b> is packed with all the essential tools to take your WooCommerce store to new heights.
            Some of the features are as follows.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-4 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="fas fa-store-alt"></i>
            </span>
            <div class="desc">
                <h3><a href="#">Complete WooCommerce Integration</a></h3>
                <p>WooCommerce is Completely integrated with HueFab Premium. Email templates, Product Page, Search Page and other WooCommerce pages will reflect your unified brand.</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="fab fa-osi"></i>
            </span>
            <div class="desc">
                <h3><a href="#">GPL3 License</a></h3>
                <p>All our themes and resources are released under GPL3 License. You will own the code.</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="fas fa-envelope-open-text"></i>
            </span>
            <div class="desc">
                <h3><a href="#">24 * 7 Support</a></h3>
                <p>With HueFab Premium, we provide the best in class support. No matter what you need, the support from our dedicated team is just a email away.</p>
            </div>
        </div>
    </div>
</div>
{{-- <div class="row">
    <div class="col-md-6 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="icon-phone"></i>
            </span>
            <div class="desc">
                <h3><a href="#">24 * 7 Support</a></h3>
                <p>With HueFab Premium, we provide the best in class support. No matter what you need, the support from our dedicated team is just a email away.</p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="icon-arrow-up-thick"></i>
            </span>
            <div class="desc">
                <h3><a href="#">Complete WooCommerce Integration</a></h3>
                <p>WooCommerce is Completely integrated with HueFab Premium. Email templates, Product Page, Search Page and other WooCommerce pages will reflect your unified brand.</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="icon-phone"></i>
            </span>
            <div class="desc">
                <h3><a href="#">5 Color Options</a></h3>
                <p>You can change complete look your store with just a single click, we have carefully designed multiple color options for you to pick best suited color theme for your website.</p>
            </div>
        </div>
    </div>
    
    <div class="col-md-6 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="icon-document"></i>
            </span>
            <div class="desc">
                <h3><a href="#">800+ Font Choices</a></h3>
                <p>With HueFab Premium you have a choice of more than 800 Google Fonts. You can change the typography of your website with just a few clicks.</p>
            </div>
        </div>
    </div>
</div> --}}