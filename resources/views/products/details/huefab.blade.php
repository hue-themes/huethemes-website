<div class="row">
    <div class="col-md-6 product-image">
        <a href="{{ URL::route('product.details', array('name'=>'huefab')) }}">
            <img class="img-responsive" src="{{URL::to('/')}}/images/img_bg_2.png"
             alt="Free HTML5 Bootstrap Template">
        </a>
        <div class="preview-bar course">
            <div class="desc">
                <span><a href="{{ URL::route('product.demo', array('name'=>'huefab-premium')) }}" class="btn btn-primary btn-sm btn-course">Live Demo</a></span>
            </div>
        </div>
    </div>
    <div class="col-md-6 animate-box product-details course">
        <a href="{{ URL::route('product.details', array('name'=>'huefab')) }}">
            <span>WordPress Theme</span>
            <h2>HueFab | WooCommerce Theme</h2>
        </a>
        <a href="{{ URL::route('product.details', array('name'=>'huefab')) }}">
            <p>HueFab is a free and open source WordPress theme with WooCommerce support. HueFab is especially suited for Ecommerce websites selling Fashion Products. HueFab is available via WordPress theme directory.</p>
        </a>
        <div class="desc">
            <span><a href="{{ URL::route('product.details', array('name'=>'huefab-premium')) }}" class="btn btn-primary btn-sm btn-course">Download</a></span>
        </div>
    </div>
</div>
<div class="row animate-box">
    <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
        <h2>Features</h2>
        <p>
            <b>HueFab Premium</b> is packed with all the essential tools to take your WooCommerce store to new heights.
            Some of the features are as follows.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-md-offset-2 col-md-4 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="fab fa-osi"></i>
            </span>
            <div class="desc">
                <h3><a href="#">GPL3 License</a></h3>
                <p>All our themes and resources are released under GPL3 License. You will own the code.</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="fas fa-envelope-open-text"></i>
            </span>
            <div class="desc">
                <h3><a href="#">24 * 7 Support</a></h3>
                <p>With HueFab Premium, we provide the best in class support. No matter what you need, the support from our dedicated team is just a email away.</p>
            </div>
        </div>
    </div>
</div>
{{-- <div class="row">
    <div class="col-md-6 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="icon-phone"></i>
            </span>
            <div class="desc">
                <h3><a href="#">24 * 7 Support</a></h3>
                <p>With HueFab Premium, we provide the best in class support. No matter what you need, the support from our dedicated team is just a email away.</p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="icon-arrow-up-thick"></i>
            </span>
            <div class="desc">
                <h3><a href="#">Complete WooCommerce Integration</a></h3>
                <p>WooCommerce is Completely integrated with HueFab Premium. Email templates, Product Page, Search Page and other WooCommerce pages will reflect your unified brand.</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="icon-phone"></i>
            </span>
            <div class="desc">
                <h3><a href="#">5 Color Options</a></h3>
                <p>You can change complete look your store with just a single click, we have carefully designed multiple color options for you to pick best suited color theme for your website.</p>
            </div>
        </div>
    </div>
    
    <div class="col-md-6 col-sm-6 text-center animate-box">
        <div class="services">
            <span class="icon">
                <i class="icon-document"></i>
            </span>
            <div class="desc">
                <h3><a href="#">800+ Font Choices</a></h3>
                <p>With HueFab Premium you have a choice of more than 800 Google Fonts. You can change the typography of your website with just a few clicks.</p>
            </div>
        </div>
    </div>
</div> --}}