<div class="course">
    <a 
        href="{{ URL::route('product.details', array('name'=>'huefab-premium')) }}"
        class="course-img"
        style="background-image: url({{URL::to('/')}}/images/project-1.png);">
    </a>
    <div class="desc">
        <h3><a href="{{ URL::route('product.details', array('name'=>'huefab-premium')) }}">HueFab Premium</a></h3>
        <p>HueFab Premium is premium WordPress theme with WooCommerce support. HueFab is especially suited for Ecommerce websites selling Fashion Products. HueFab Premium also includes excellent commercial support.</p>
        <span><a href="{{ URL::route('product.details', array('name'=>'huefab-premium')) }}" class="btn btn-primary btn-sm btn-course">Details</a></span>
    </div>
</div>