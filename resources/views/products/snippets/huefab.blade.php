<div class="course">
<a href="{{ URL::route('product.details', array('name'=>'huefab')) }}"
    class="course-img"
    style="background-image: url({{URL::to('/')}}/images/project-1.png);">
    </a>
    <div class="desc">
        <h3><a href="{{ URL::route('product.details', array('name'=>'huefab')) }}">HueFab</a></h3>
        <p>HueFab is a free and open source WordPress theme with WooCommerce support. HueFab is especially suited for Ecommerce websites selling Fashion Products. HueFab is available via WordPress theme directory.</p>
        <span><a href="{{ URL::route('product.details', array('name'=>'huefab')) }}" class="btn btn-primary btn-sm btn-course">Details</a></span>
    </div>
</div>