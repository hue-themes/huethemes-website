@extends('layouts.app')

@section('content')
<div id="fh5co-about">
	<div class="container">
		<div class="row">
			@include('products.featured.huefab-premium')
		</div>
	</div>
</div>
<div id="fh5co-course">
	<div class="container">
		<div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
				<h2>Browse Products</h2>
				<p>Checkout our latest themes and plugins.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 animate-box">
				@include('products.snippets.huefab-premium')
			</div>
			<div class="col-md-6 animate-box">
				@include('products.snippets.huefab')
			</div>
		</div>
	</div>
</div>
@endsection
