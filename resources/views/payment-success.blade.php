@extends('layouts.app')

@section('content')
<div id="fh5co-about">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center fh5co-heading subscription-box">
				<span class="icon">
					<i class="fas fa-check-circle"></i>
				</span>
				<h2>Subscription Successful</h2>
				@include('products.payment-success.'.$productName)
				<p>
					Please reach out to <a href="mailto:support@huethemes.com">support@huethemes.com</a> for any query or feedback.
				</p>
			</div>
		</div>
	</div>
</div>
@endsection
