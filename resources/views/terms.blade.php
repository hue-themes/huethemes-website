@extends('layouts.app')

@section('content')
<div id="fh5co-contact">
	<div class="container">
		<div class="row">
            <div class="col-md-12">
                <h2>Terms and Conditions</h2>
                <hr/>
                <div class="article-content">
                    <p><strong>1 &nbsp;Acceptance The Use Of HueThemes Terms and Conditions</strong></p>
                    <p>Your  access  to  and  use  of  HueThemes is  subject exclusively to these Terms and Conditions. You will not use the Website for any purpose that is unlawful or prohibited by these Terms and Conditions. By using  the  Website  you  are  fully  accepting  the  terms,  conditions  and disclaimers contained in this notice. If you do not accept these Terms and Conditions you must immediately stop using the Website.</p>

                    <p><strong>2 &nbsp;Advice</strong></p>
                    <p>The contents of HueThemes website do not constitute advice and should not be relied upon in making or refraining from making, any decision.</p>

                    <p><strong>3 &nbsp;Change of Use</strong></p>
                    <p>HueThemes reserves the right to:<br /> 4.1 &nbsp;change or remove (temporarily or permanently) the Website or any part of it without notice and you confirm that HueThemes shall not be liable to you for any such change or removal and.<br /> 4.2 &nbsp;change these Terms and Conditions at any time, and your continued use of the Website following any changes shall be deemed to be your acceptance of such change.</p>

                    <p><strong>4 &nbsp;Links to Third Party Websites</strong></p>
                    <p>HueThemes Website may include links to third party websites that are controlled and maintained by others. Any link to other websites is not an endorsement of such websites and you acknowledge and agree that we are not responsible for the content or availability of any such sites.</p>

                    <p><strong>5 &nbsp;Copyright </strong></p>
                    <p>6.1 &nbsp;All  copyright,  trade  marks  and  all  other  intellectual  property  rights  in  the Website and its content (including without limitation the Website design, text, graphics and all software and source codes connected with the Website) are owned by or   licensed to HueThemes or otherwise used by HueThemes as permitted by law.<br /> 6.2 &nbsp;In accessing the Website you agree that you will access the content solely for your personal, non-commercial use. None of the content may be downloaded, copied, reproduced, transmitted, stored, sold or distributed without the prior written consent of the copyright holder. This excludes the downloading, copying and/or printing of pages of the Website for personal, non-commercial home use only.</p>

                    <p><strong>6 &nbsp;Disclaimers and Limitation of Liability </strong></p>
                    <p>7.1 &nbsp;The Website is provided on an AS IS and AS AVAILABLE basis without any representation or endorsement made and without warranty of any kind whether express or implied, including but not limited to the implied warranties of satisfactory quality, fitness for a particular purpose, non-infringement, compatibility, security and accuracy.<br /> 7.2 &nbsp;To the extent permitted by law, HueThemes will not be liable for any indirect or consequential loss or damage whatever (including without limitation loss of business, opportunity, data, profits) arising out of or in connection with the use of the Website.<br /> 7.3 &nbsp;HueThemes makes no warranty that the functionality of the Website will be uninterrupted or error free, that defects will be corrected or that the Website or the server that makes it available are free of viruses or anything else which may be harmful or destructive.<br /> 7.4 &nbsp;Nothing in these Terms and Conditions shall be construed so as to exclude or limit the liability of HueThemes for death or personal injury as a result of the negligence of HueThemes or that of its employees or agents.</p>

                    <p><strong>7 &nbsp;Indemnity</strong></p>
                    <p>You agree to indemnify and hold HueThemes and its employees and agents harmless from and against all liabilities, legal fees, damages, losses, costs and other expenses in relation to any claims or actions brought against HueThemes arising out of any breach by you of these Terms and Conditions or other liabilities arising out of your use of this Website.</p>

                    <p><strong>8 &nbsp;Severance</strong></p>
                    <p>If any of these Terms and Conditions should be determined to be invalid, illegal or unenforceable for any reason by any court of competent jurisdiction then such Term or Condition shall be severed and the remaining Terms and Conditions shall survive and remain in full force and effect and continue to be binding and enforceable.</p>

                    <p><strong>9 &nbsp;Governing Law</strong></p>
                    <p>These Terms and Conditions shall be governed by and construed in accordance with the law of India.</p>

                    For any further information please email <a href='mailto:support@huethemes.in'> our support team.</a>
                </div>
            </div>
		</div>
		
	</div>
</div>
@endsection
