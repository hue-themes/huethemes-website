@extends('layouts.app')

@section('content')
<section class="cd-hero js-cd-hero js-cd-autoplay">
    <ul class="cd-hero__slider">
        <li class="cd-hero__slide cd-hero__slide--selected js-cd-slide">
            <div class="cd-hero__content cd-hero__content--full-width">
                <h2>Explore our products</h2>
                <p>Checkout our beautiful themes and plugins.</p>
            <a href="{{URL::route('products')}}" class="cd-hero__btn">Browse Themes</a>
            </div> <!-- .cd-hero__content -->
        </li>

        <li class="cd-hero__slide js-cd-slide">
            <div class="cd-hero__content cd-hero__content--half-width">
                <h2>HueFab Premium for WordPress</h2>
                <p>Mobile first, responsive modern theme for WordPress.</p>
                <a href="{{URL::route('product.details', ['name'=>'huefab-premium'])}}" class="cd-hero__btn">Details</a>
                <a href="{{URL::route('product.demo', ['name'=>'huefab-premium'])}}" class="cd-hero__btn cd-hero__btn--secondary">Live Demo</a>
            </div> <!-- .cd-hero__content -->

            <div class="cd-hero__content cd-hero__content--half-width cd-hero__content--img">
                <img src="assets/tech-1.png" alt="tech 1">
            </div> <!-- .cd-hero__content -->
        </li>

        <li class="cd-hero__slide js-cd-slide">
            <div class="cd-hero__content cd-hero__content--half-width">
                <h2>HueFab for WordPress</h2>
                <p>Mobile first, responsive modern theme for WordPress.</p>
                <a href="{{URL::route('product.details', ['name'=>'huefab'])}}" class="cd-hero__btn">Details</a>
                <a href="{{URL::route('product.demo', ['name'=>'huefab'])}}" class="cd-hero__btn cd-hero__btn--secondary">Live Demo</a>
            </div> <!-- .cd-hero__content -->

            <div class="cd-hero__content cd-hero__content--half-width cd-hero__content--img">
                <img src="assets/tech-2.png" alt="tech 1">
            </div> <!-- .cd-hero__content -->
        </li>
    </ul> <!-- .cd-hero__slider -->

    <div class="cd-hero__nav js-cd-nav">
        <nav>
            <span class="cd-hero__marker cd-hero__marker--item-1 js-cd-marker"></span>
            
            <ul>
                <li class="cd-selected"><a href="#0">Themes</a></li>
                <li><a href="#0">HueFab Premium</a></li>
                <li><a href="#0">HueFab</a></li>
            </ul>
        </nav> 
    </div> <!-- .cd-hero__nav -->
</section> <!-- .cd-hero -->
<div id="fh5co-course-categories">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                <h2>Why choose HueThemes?</h2>
                <p>We build modern mobile first responsive websites.
                    We like to put you in command and with our powerful admin panel you can do just that!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-md-offset-2 text-center animate-box">
                <div class="services">
                    <span class="icon">
                        <i class="fas fa-envelope-open-text"></i>
                    </span>
                    <div class="desc">
                        <h3><a href="#">24 * 7 Support</a></h3>
                        <p>We provide you 24 * 7 support in order for you to achieve maximum value out of our products.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 text-center animate-box">
                <div class="services">
                    <span class="icon">
                        <i class="fas fa-code"></i>
                    </span>
                    <div class="desc">
                        <h3><a href="#">Coding Standards</a></h3>
                        <p>We follow strict coding stadards in order to make our themes compatible with all WordPress features and plugins.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="fh5co-course">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                <h2>Browse Themes</h2>
                <p>Checkout our latest themes and plugins.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 animate-box">
                @include('products.snippets.huefab')
            </div>
            <div class="col-md-6 animate-box">
                @include('products.snippets.huefab-premium')
            </div>
        </div>
    </div>
</div>
@endsection
